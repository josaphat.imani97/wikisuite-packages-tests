<?php
/**
 * This is a PHPUnit based test suite to test apache and nginx configuration
 *
 * It takes the rules from Tiki .htaccess and attempts to craft tests to exercise
 * these rules. Expectation is that all test will pass in any webserver properly
 * configured
 *
 * => Before running this tests, you need to run the following commands on the server to test:
 *
 * # Install Diagram package:
 * php console.php package:install diagram
 * # Create a test folder
 * mkdir test-web-browse && touch test-web-browse/{.hidden,file,js.js,png.png,README}
 *
 * => To run this tool from the commands line:
 *
 * 1. Download PHPUnit phar file from https://phar.phpunit.de/phpunit-9.phar
 * 2. Rename the file as phpunit.phar (optional)
 * 3. Execute the command below, replace the value of WEBSERVER with the host to use for testing
 *
 * WEBSERVER='https://server.example.com' php phpunit.phar WebserverRulesTest.php
 *
 */
use PHPUnit\Framework\TestCase;

class RequestUrl
{
    protected $headers;
    protected $headersOut;
    protected $body;
    protected $code;

    static public function call($url, $verb = 'GET', $compressed = false, $extraHeaders = []) : RequestUrl
    {
        $ch = curl_init();
        $headers = [];

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        if ( $compressed ) {
            curl_setopt($ch, CURLOPT_ENCODING, '');
        }

        if ($verb != 'GET') {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $verb);
        }

        if ( ! empty($extraHeaders) ) {
            curl_setopt($ch,CURLOPT_HTTPHEADER, $extraHeaders);
        }

        // this function is called by curl for each header received
        curl_setopt($ch, CURLOPT_HEADERFUNCTION,
            function($curl, $header) use (&$headers)
            {
                $len = strlen($header);
                $header = explode(':', $header, 2);
                if (count($header) < 2) // ignore invalid headers
                    return $len;

                $headers[strtolower(trim($header[0]))][] = trim($header[1]);

                return $len;
            }
        );

        $data = curl_exec($ch);

        $result = new static();
        $result->headers = $headers;
        $result->headersOut = curl_getinfo($ch, CURLINFO_HEADER_OUT);
        $result->body = $data;
        $result->code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        return $result;
    }

    public function getHeaders()
    {
        return $this->headers;
    }

    public function getHeadersOut()
    {
        return $this->headersOut;
    }

    public function getHeader($header)
    {
        $header = strtolower($header);

        if ( ! isset($this->headers[$header]) ) {
            return null;
        }
        return implode(',', $this->headers[$header]);
    }

    public function getBody()
    {
        return $this->body;
    }

    public function getCode()
    {
        return $this->code;
    }

}

class WebserverRulesTest extends TestCase
{
    protected string $host;

    protected function setUp(): void
    {
        $this->host = getenv('WEBSERVER');
    }

    /**
     * @param string $url
     * @param int $code
     * @param string $verb
     * @return void
     *
     * @dataProvider getRequestsProvider
     */
    public function testGetRequests(string $url, int $code, string $verb='GET'): void
    {
        $result = RequestUrl::call($this->host . $url, $verb);
        $this->assertEquals($code, $result->getCode());
    }

    public function getRequestsProvider(): array
    {
        $urls =  [
            # base URLS - GET
            ['/tiki-index.php', 200],
            ['/index.php', 302],
            ['/', 302],

            # -- Prevent Browsing of Certain File Extensions -- # - GET
            ['/phpcs.xml.dist', 403],

            # -- Prevent Browsing of Certain File Names -- # - GET
            ['/changelog.txt', 403],

            # -- Home Page Feature -- # - GET
            ['/HomePage', 200],

            # -- Ensure Caching Proxy Sends Content to Correct Client -- # - Header
            # Header
            # -- Client Cache Method -- # - Header
            # Header
            # -- Client Cache Expiration -- # - Header
            # Header
            # -- Persistent Connections -- # - Header
            # Header
            # -- CORS-enabled Images (@crossorigin) -- # - Header
            # Header
            # -- Webfont Access -- # - Header
            # Header
            # -- Enable video/audio seek -- # - Header
            # Header
            # -- Expires Headers -- # - Header
            # Header

            # Redirect drawio requests to the correct path - GET
            ['/img/lib/mscae/Backup.svg', 200],

            # -- Apache Authorization Header -- # ????
            # -- Set these headers for Last-Modified and Etag in wiki pages -- # ????

            # -- Prevent HTTP TRACE method -- # - GET
            ['/', 405, 'TRACE'],

            # -- Disallow access to hidden files (apart from well-known ones) -- # - GET
            ['/.gitignore', 403],

            # Redirect robots.txt to dynamic tiki-robots.php file. - GET
            ['/robots.txt', 200],

            # -- If the URL Points to a File Then do Nothing -- # - GET

            # -- CalDAV/CardDAV service auto-discovery -- # - GET
            ['/.well-known/caldav', 301],
            ['/.well-known/carddav', 301],

            # -- Tiki URL Rewriting -- # - GET
            ['/HomePage', 200],

            # -- Prevent Directory Browsing -- # - GET
            ['/test-web-browse/', 403],
            ['/test-web-browse/.hidden', 403],
            ['/test-web-browse/js.js', 200],

            # Htaccess in temp folder ( deny all, allow .png, .html, .js, .css, .pdf ) - GET
            ['/temp', 403],
            ['/temp/composer.phar', 403],
            ['/temp/templates_c', 403],
            ['/temp/public/codemirror_modes.js', 200],
            ['/temp/public/codemirror_modes.css', 200],

        ];

        return $urls;
    }

    /**
     * @param string $url
     * @param bool $compressed
     * @param array $extraHeaders
     * @param string $header
     * @param string|null $value
     * @return void
     * @dataProvider getHeadersProvider
     */
    public function testGetHeaders(string $url, bool $compressed, array $extraHeaders, string $header, ?string $value): void
    {
        $result = RequestUrl::call($this->host . $url, 'GET', $compressed, $extraHeaders);
        //var_dump($result->getHeadersOut());
        //var_dump($result->getHeaders());
        $this->assertEquals($value, $result->getHeader($header));
    }

    public function getHeadersProvider(): array
    {
        $urls =  [
            # base URLS - GET
            # -- Prevent Browsing of Certain File Extensions -- # - GET
            # -- Prevent Browsing of Certain File Names -- # - GET
            # -- Home Page Feature -- # - GET
            # -- Ensure Caching Proxy Sends Content to Correct Client -- #

            ['/themes/yeti/css/yeti.css', false, [], 'Vary', 'Accept-Encoding'],
            ['/themes/yeti/css/yeti.css', false, [], 'Content-Encoding', null],
            ['/themes/yeti/css/yeti.css', true, [], 'Vary', 'Accept-Encoding'],
            ['/themes/yeti/css/yeti.css', true, [], 'Content-Encoding', 'gzip'],

            # -- Client Cache Method -- #
            # Header
            # -- Client Cache Expiration -- #
            # Header
            # -- Persistent Connections -- #
            # Header
            # -- CORS-enabled Images (@crossorigin) -- #
            ['/themes/yeti/css/yeti.css', false, ['Origin: www.example.com'], 'Access-Control-Allow-Origin', null],
            ['/themes/yeti/images/yeti.png', false, ['Origin: www.example.com'], 'Access-Control-Allow-Origin', '*'],
            # Header
            # -- Webfont Access -- #
            ['/lib/pdf/fontdata/fontttf/fontawesome.ttf', false, ['Origin: www.example.com'], 'Access-Control-Allow-Origin', '*'],
            # -- Enable video/audio seek -- #
            ['/themes/yeti/css/yeti.css', true, [], 'Accept-Ranges', 'bytes'],
            # -- Expires Headers -- #
            # Header
            # Redirect drawio requests to the correct path

            # -- Apache Authorization Header -- #
            # Header
            # -- Set these headers for Last-Modified and Etag in wiki pages -- #
            # -- Prevent HTTP TRACE method -- #
            # -- Disallow access to hidden files (apart from well-known ones) -- #
            # Redirect robots.txt to dynamic tiki-robots.php file.
            # -- If the URL Points to a File Then do Nothing -- #
            # -- CalDAV/CardDAV service auto-discovery -- #
            # -- Tiki URL Rewriting -- #
            # -- Prevent Directory Browsing -- #

        ];

        return $urls;
    }

}