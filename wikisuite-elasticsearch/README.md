The Elasticsearch package in WikiSuite is deprecated because Elasticsearch is no longer Open Source. It is replaced by Manticore Search:
https://gitlab.com/wikisuite/wikisuite-packages/-/tree/main/wikisuite-manticore

Tiki still supports Elasticsearch, and you just need to install it according to your requirements.
https://doc.tiki.org/Elasticsearch#Support_matrix

Please see:
https://wikisuite.org/Elasticsearch
